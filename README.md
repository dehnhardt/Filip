# Filip

## Simple phase flip plugin (LV2)
This has no GUI at all. If it's enabled, it flips the phase, if not then not ;-)

## Compiling
You need the usual compiling tools and the package lv2-dev.
Clone the sourcecode (git clone https://codeberg.org/dehnhardt/Filip.git), move to the directory and type 'make'

## Installing
In the sourcecode directory type 'sudo make install'

## Not Compiling ;-)
In the directory mkl-filip.lv2 is a compiled version for a 64 bit Linux. You can try to copy this into your 
* /usr/local/lib/lv2   
or  
* /usr/lib/lv2  
directory, depending on your Linux distribution. This might work for recent Linux systems.

## Disclaimer
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
