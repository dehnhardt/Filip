#pragma once

#include <cstdint>

#define DB_CO(g) ((g) > -90.0f ? powf(10.0f, (g)*0.05f) : 0.0f)

typedef enum { 
    FILIP_INPUT_L = 0, 
    FILIP_INPUT_R = 1, 
    FILIP_OUTPUT_L = 2, 
    FILIP_OUTPUT_R = 3
} PortIndex;

class Filip
{
    public:

        Filip() = default;
        ~Filip() = default;
        void run (const uint32_t sample_count);
        void connectPort (const uint32_t port, void* data);

    private: 
        float* input_l;
        float* input_r;
        float* output_l;
        float* output_r;
};
