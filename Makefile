BUNDLE = mkl-filip.lv2
INSTALL_DIR = /usr/local/lib/lv2


$(BUNDLE): manifest.ttl mkl-filip.ttl mkl-filip.so
	rm -rf $(BUNDLE)
	mkdir $(BUNDLE)
	cp manifest.ttl mkl-filip.ttl mkl-filip.so $(BUNDLE)

mkl-filip.so: mkl-filip.cpp
	g++ -shared -fPIC -DPIC mkl-filip.cpp `pkg-config --cflags --libs lv2-plugin` -o mkl-filip.so

install: $(BUNDLE)
	if [ -d "/usr/lib/lv2" ]; then echo "Dir INSTALL_DIR=/usr/lib exists"; INSTALL_DIR=/usr/lib/lv2; fi
	if [ -d "/usr/local/lib/lv2" ]; then echo "Dir /usr/local/lib exists"; INSTALL_DIR=/usr/local/lib/lv2; fi
	mkdir -p $(INSTALL_DIR)
	rm -rf $(INSTALL_DIR)/$(BUNDLE)
	cp -R $(BUNDLE) $(INSTALL_DIR)

clean:
	rm -rf $(BUNDLE) mkl-filip.so
