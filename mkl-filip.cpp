#include "lv2/core/lv2.h"
#include <cmath>
#include <stdlib.h>
#include <iostream>

#include "mkl-filip.h"

#define FILIP_URI "http://punkt-k.de/plugins/mkl-filip"

void Filip::connectPort( const uint32_t port, void* data )
{
  switch( port )
  {
    case FILIP_INPUT_L:
      input_l = (float*)data;
      break;
    case FILIP_INPUT_R:
      input_r = (float*)data;
      break;
    case FILIP_OUTPUT_L:
      output_l = (float*)data;
      break;
    case FILIP_OUTPUT_R:
      output_r = (float*)data;
      break;
  }
}

void Filip::run (const uint32_t sample_count) 
{
  for (uint32_t pos = 0; pos < sample_count; pos++)
  {
        output_l[pos] = input_l[pos] * -1;
        output_r[pos] = input_r[pos] * -1;
  }
}

static LV2_Handle
instantiate(const LV2_Descriptor*     descriptor,
            double                    rate,
            const char*               bundle_path,
            const LV2_Feature* const* features)
{

    Filip* filip = nullptr;
    try 
    {
        filip = new Filip ();
    } 
    catch (const std::bad_alloc& ba)
    {
        std::cerr << "Failed to allocate memory. Can't instantiate MKL-Filip" << std::endl;
        return nullptr;
    }
    return filip;
}

// LV2 Methods

static void
connect_port(LV2_Handle instance, uint32_t port, void* data)
{
  Filip* filip = static_cast< Filip* >( instance );
  if( filip != nullptr) 
    filip->connectPort( port, data );
}


static void
activate(LV2_Handle instance)
{}


static void
run(LV2_Handle instance, uint32_t n_samples)
{
  Filip* filip = static_cast< Filip* >( instance );
  if( filip != nullptr) 
    filip->run( n_samples );

}

static void
deactivate(LV2_Handle instance)
{}

static void
cleanup(LV2_Handle instance)
{
  Filip* filip = static_cast< Filip* >( instance );
  if( filip != nullptr) 
    delete filip;
}

static const void*
extension_data(const char* uri)
{
  return NULL;
}

static const LV2_Descriptor descriptor = {FILIP_URI,
                                          instantiate,
                                          connect_port,
                                          activate,
                                          run,
                                          deactivate,
                                          cleanup,
                                          extension_data };

LV2_SYMBOL_EXPORT
const LV2_Descriptor*
lv2_descriptor(uint32_t index)
{
  return index == 0 ? &descriptor : NULL;
}